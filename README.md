# Website Scanner
This simple python script allows anyone to paste a list of urls/sites and the script will simply visit the given URL, Screenshot & Log the URL it redirects to if it does.

This was used for Scambaiting/Educational purposes for finding Pop ups from a list of sketchy URL's in my case.

It is not perfect and very alpha, any feedback is appreciated. 

If you would like to get in touch with me, please email hello@joshuapike.uk or add me on Discord. User: senpikey

This project is under the GNU GENERAL PUBLIC LICENSE
https://www.gnu.org/licenses/gpl-3.0.en.html
