import time
import os
import shutil
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import zipfile
from datetime import datetime

import random
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service  

# Configure the Chrome options for running headless
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--window-size=1920x1080")

# Initialize the web driver with options
service = Service(executable_path=r"ADD PATH")
options = webdriver.ChromeOptions()
driver = webdriver.Chrome(service=service, options=chrome_options)

# Read the list of websites to scan from a text file
websites_to_scan = []
with open("websites.txt", "r") as file:
    websites_to_scan = [line.strip() for line in file]

# Create directories to store visited URLs, screenshots, and redirect logs
if not os.path.exists("visited_urls"):
    os.makedirs("visited_urls")
if not os.path.exists("screenshots"):
    os.makedirs("screenshots")

# Initialize counters for naming screenshots and logs
screenshot_counter = 1
log_counter = 1

# Define a function to follow redirects, log URLs, and take screenshots
def follow_redirects(url):
    global screenshot_counter, log_counter
    try:
        driver.get(url)

        # Get the final URL after redirection
        final_url = driver.current_url

        # Log the final URL to a file
        log_filename = f"visited_urls/visited_url_{log_counter}.txt"
        with open(log_filename, "w") as log_file:
            log_file.write(f"Original URL: {url}\n")
            log_file.write(f"Final URL: {final_url}\n")

        # Take a screenshot with a filename corresponding to the screenshot counter
        screenshot_filename = f"screenshots/screenshot_{screenshot_counter}.png"
        driver.save_screenshot(screenshot_filename)

        print(f"Visited URL: {final_url} (Screenshot saved as {screenshot_filename})")

        # Increment the counters
        screenshot_counter += 1
        log_counter += 1

    except Exception as e:
        # Handle the exception and continue the script
        print(f"Error: {str(e)}. Skipping URL: {url}")

# Run the scanning process
while True:
    for website in websites_to_scan:
        follow_redirects(website)

    print("Creating Copy")
    current_datetime = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    # Create a dynamic zip file name based on the current date and time
    zip_filename = f"output_{current_datetime}.zip"

    # Compress screenshots and log files into the dynamic zip archive
    shutil.make_archive(zip_filename[:-4], "zip", ".", "screenshots", "visited_urls")

    # Move the dynamic zip archive to a specific destination directory
    destination_path =r"ADD PATH"  # Replace with your desired destination path
    shutil.move(f"{zip_filename}", os.path.join(destination_path, zip_filename))

    # Clean up the temporary directories
    shutil.rmtree("screenshots")
    shutil.rmtree("visited_urls")
    os.makedirs("screenshots")
    os.makedirs("visited_urls")
    # Sleep for a specified interval before visiting again (e.g., every 1-5 minutes)
    sleep_time = 300  # Adjust the interval as needed
    print(f"Sleeping for {sleep_time} seconds...")
    time.sleep(sleep_time)

# Quit the driver when done
driver.quit()
 